// MongoDB Aggregation

db.course_bookings.insertMany([
    {
        "courseId" : "C001", 
        "studentId": "S004", 
        "isCompleted": true
    },
    {
        "courseId" : "C002", 
        "studentId": "S001", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId" : "C003", 
        "studentId": "S002", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId" : "C004", 
        "studentId": "S004", 
        "isCompleted": false
    },
    {
        "courseId" : "C002", 
        "studentId": "S007", 
        "isCompleted": true
    },
    {
        "courseId" : "C003", 
        "studentId": "S005", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S008", 
        "isCompleted": true
    },
    {
        "courseId" : "C004", 
        "studentId": "S0013", 
        "isCompleted": false
    }
]);




/*
	Syntax:
	db.collectionName.aggregate([
		{
			{
 			 $group:
    				{
      				_id: <expression>, // Group key
     				 <field1>: { <accumulator1> : <expression1> },
      
    			}
			 }
		}
	])

*/

// Aggregation allows us to retrieve a group of data based on specific conditions. In this case, we are retrieving or grouping the data inside our course-booking table getting the total count of all the data inside of it.
db.course_bookings.aggregate([
	{
		$group: {_id: null, count: {$sum: 1}}
	}
]);

// $match is a condition that has to be met in order for MongoDB to return data. In this case, we are trying to get all the fields where "isCompleted" is equal to true.
db.course_bookings.aggregate([
	{
		$match: {"isCompleted": true}
	},
	{
		$group: {_id: "$courseId", total: {$sum: 1}}
	}
]);

// $project basically either shows or doesn't show a field.
db.course_bookings.aggregate([
	{
		$match: {"isCompleted": true}
	},
	{
		$project: {"studentId": 1}
	}
]);
// $sort organizes the returned data/documents in ascending(1) or descending order (-1).

db.course_bookings.aggregate([
	{
		$match: {"isCompleted": true}
	},
	{
		$sort: {"courseId": -1}
	}
]);







db.course_bookings.aggregate([
	{
		$match: {"isCompleted": true}
	},
	{
		$group: {_id: "S0013", count:{$sum:1}}
	}
]);


db.course_bookings.aggregate([
	{
		$match: {"isCompleted": true}
	},
	{
		$sort: {"courseId":-1, "studentId": 1}
	}
]);


db.orders.insertMany([
	{
		"customer_Id": "A123",
		"amount": 500,
		"status": "A"
	},
	{
		"customer_Id": "A123",
		"amount": 250,
		"status": "A"
	},
	{
		"customer_Id": "B212",
		"amount": 200,
		"status": "A"
	},
	{
		"customer_Id": "B212",
		"amount": 200,
		"status": "D"
	},
]);

// Operators are a way to do automatic calculation within our query.

/*
	1. $sum - Returns a sum of numerical values. Ignores non-numeric values.
	2. $max - Returns the highest expression value for each group.
	3. $min - Returns the lowest expression value for each group.
	4. $avg - Returns an average of numerical values. Ignores non-numeric values. 
*/

// $max operator returns the highest value of data
db.orders.aggregate([
	{
		$match: {"status": "A"}
	},
	{
		$group: {_id: "$customer_Id", maxAmount: {$max: "$amount"}}
	}
]);

// $min operator
db.orders.aggregate([
	{
		$match: {"status": "A"}
	},
	{
		$group: {_id: "$customer_Id", minAmount: {$min: "$amount"}}
	}
]);

// $avg operator
db.orders.aggregate([
	{
		$match: {"status": "A"}
	},
	{
		$group: {_id: "$customer_Id", avgAmount: {$avg: "$amount"}}
	}
]);